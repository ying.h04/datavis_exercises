import { writable } from 'svelte/store';
import { scaleLinear } from 'd3-scale';


export const day_width = 20;
export const day_height = 20;
export const xScale = scaleLinear().domain([0,24]).range([0,day_width]);
export const yScale = writable();
export const yScale_down = scaleLinear().domain([0,3800]).range([day_height,day_height*2]);
export const scale_chosen = writable("across all building types");
export const city_chosen = writable("Baltimore");

export const yScale_0 = scaleLinear().domain([0,220]).range([day_height,0]);
export const yScale_1 = scaleLinear().domain([0,1600]).range([day_height,0]);
export const yScale_2 = scaleLinear().domain([0,650]).range([day_height,0]);
export const yScale_3 = scaleLinear().domain([0,3800]).range([day_height,0]);
export const yScale_4 = scaleLinear().domain([0,200]).range([day_height,0]);
export const yScale_5 = scaleLinear().domain([0,210]).range([day_height,0]);
export const yScale_6 = scaleLinear().domain([0,400]).range([day_height,0]);
export const yScale_7 = scaleLinear().domain([0,1200]).range([day_height,0]);
export const yScale_8 = scaleLinear().domain([0,120]).range([day_height,0]);
export const yScale_9 = scaleLinear().domain([0,5000]).range([day_height,0]);
export const yScale_10 = scaleLinear().domain([0,110]).range([day_height,0]);
export const yScale_11 = scaleLinear().domain([0,50]).range([day_height,0]);
export const yScale_12 = scaleLinear().domain([0,350]).range([day_height,0]);
export const yScale_13 = scaleLinear().domain([0,350]).range([day_height,0]);
export const yScale_14 = scaleLinear().domain([0,1000]).range([day_height,0]);
export const yScale_15 = scaleLinear().domain([0,400]).range([day_height,0]);

export const yScale_0_d = scaleLinear().domain([0,220]).range([day_height,day_height*2]);
export const yScale_1_d = scaleLinear().domain([0,1600]).range([day_height,day_height*2]);
export const yScale_2_d = scaleLinear().domain([0,650]).range([day_height,day_height*2]);
export const yScale_3_d = scaleLinear().domain([0,3800]).range([day_height,day_height*2]);
export const yScale_4_d = scaleLinear().domain([0,200]).range([day_height,day_height*2]);
export const yScale_5_d = scaleLinear().domain([0,210]).range([day_height,day_height*2]);
export const yScale_6_d = scaleLinear().domain([0,400]).range([day_height,day_height*2]);
export const yScale_7_d = scaleLinear().domain([0,1200]).range([day_height,day_height*2]);
export const yScale_8_d = scaleLinear().domain([0,120]).range([day_height,day_height*2]);
export const yScale_9_d = scaleLinear().domain([0,5000]).range([day_height,day_height*2]);
export const yScale_10_d = scaleLinear().domain([0,110]).range([day_height,day_height*2]);
export const yScale_11_d = scaleLinear().domain([0,50]).range([day_height,day_height*2]);
export const yScale_12_d = scaleLinear().domain([0,350]).range([day_height,day_height*2]);
export const yScale_13_d = scaleLinear().domain([0,350]).range([day_height,day_height*2]);
export const yScale_14_d = scaleLinear().domain([0,1000]).range([day_height,day_height*2]);
export const yScale_15_d = scaleLinear().domain([0,400]).range([day_height,day_height*2]);


export const yScale_0_la = scaleLinear().domain([0,60]).range([day_height,0]);
export const yScale_1_la = scaleLinear().domain([0,1300]).range([day_height,0]);
export const yScale_2_la = scaleLinear().domain([0,100]).range([day_height,0]);
export const yScale_3_la = scaleLinear().domain([0,320]).range([day_height,0]);
export const yScale_4_la = scaleLinear().domain([0,0.12]).range([day_height,0]);
export const yScale_5_la = scaleLinear().domain([0,18]).range([day_height,0]);
export const yScale_6_la = scaleLinear().domain([0,250]).range([day_height,0]);
export const yScale_7_la = scaleLinear().domain([0,290]).range([day_height,0]);
export const yScale_8_la = scaleLinear().domain([0,30]).range([day_height,0]);
export const yScale_9_la = scaleLinear().domain([0,1200]).range([day_height,0]);
export const yScale_10_la = scaleLinear().domain([0,20]).range([day_height,0]);
export const yScale_11_la = scaleLinear().domain([0,3]).range([day_height,0]);
export const yScale_12_la = scaleLinear().domain([0,150]).range([day_height,0]);
export const yScale_13_la = scaleLinear().domain([0,140]).range([day_height,0]);
export const yScale_14_la = scaleLinear().domain([0,300]).range([day_height,0]);
export const yScale_15_la = scaleLinear().domain([0,100]).range([day_height,0])

export const yScale_0_la_d = scaleLinear().domain([0,60]).range([day_height,day_height*2]);
export const yScale_1_la_d = scaleLinear().domain([0,1300]).range([day_height,day_height*2]);
export const yScale_2_la_d = scaleLinear().domain([0,100]).range([day_height,day_height*2]);
export const yScale_3_la_d = scaleLinear().domain([0,320]).range([day_height,day_height*2]);
export const yScale_4_la_d = scaleLinear().domain([0,0.12]).range([day_height,day_height*2]);
export const yScale_5_la_d = scaleLinear().domain([0,18]).range([day_height,day_height*2]);
export const yScale_6_la_d = scaleLinear().domain([0,250]).range([day_height,day_height*2]);
export const yScale_7_la_d = scaleLinear().domain([0,290]).range([day_height,day_height*2]);
export const yScale_8_la_d = scaleLinear().domain([0,30]).range([day_height,day_height*2]);
export const yScale_9_la_d = scaleLinear().domain([0,1200]).range([day_height,day_height*2]);
export const yScale_10_la_d = scaleLinear().domain([0,20]).range([day_height,day_height*2]);
export const yScale_11_la_d = scaleLinear().domain([0,3]).range([day_height,day_height*2]);
export const yScale_12_la_d = scaleLinear().domain([0,150]).range([day_height,day_height*2]);
export const yScale_13_la_d = scaleLinear().domain([0,140]).range([day_height,day_height*2]);
export const yScale_14_la_d = scaleLinear().domain([0,300]).range([day_height,day_height*2]);
export const yScale_15_la_d = scaleLinear().domain([0,100]).range([day_height,day_height*2])