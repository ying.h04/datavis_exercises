import { writable } from 'svelte/store';
import { scaleLinear } from 'd3-scale';


export const day_width = 30;
export const leaf_length = 210;
export const leaf_hwidth = 70;
export const xScale_r = scaleLinear().domain([0,24]).range([0,day_width])
export const xScale_l = scaleLinear().domain([0,24]).range([0,-day_width])
export const yScale_e = scaleLinear().domain([0,1100]).range([leaf_hwidth,0])
export const yScale_g = scaleLinear().domain([0,4700]).range([leaf_hwidth,2*leaf_hwidth])

