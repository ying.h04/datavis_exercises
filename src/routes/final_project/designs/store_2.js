import {scaleLinear} from "d3-scale";


export const clock_radius = 120;

export const yearly = scaleLinear().domain([1,365+1]).range([0,2*Math.PI]);
export const scaleRadius_h = scaleLinear().domain([0,160000]).range([0,clock_radius]);
export const scaleRadius_c = scaleLinear().domain([0,54000]).range([0,clock_radius]);
